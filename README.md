# EasyPHP vHost Management

#### Installation:

1. Go to EasyPHP installation folder ( probably `C:\Program Files (x86)\EasyPHP-Devserver-17` ).
2. Go to `eds-dashboard` folder.
3. Put the php files in there.
4. Open `index.php` in the editor (look at `index.example.php`)
    Add this to begining of the file: (somewhere near line #60)
    ```php
    include_once dirname(__FILE__ ) . '/rd-vhost-handler.php';
    ```
    Add this just above working directories html: (somewhere near line #430)
    ```php
	include dirname(__FILE__ ) . '/rd-vhost-editor.php';
	```
    
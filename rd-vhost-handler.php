<?php
// Delete alias
if (isset($_POST['vhost_delete'])) {
	$vhost_serialized = file_get_contents('store_vhosts.php');
	$vhost_array = array();
	$vhost_array = unserialize($vhost_serialized);
	unset($vhost_array[$_POST['vhost_delete']]);
	file_put_contents('store_vhosts.php',serialize($vhost_array));
	header('Location: index.php'); 
	exit;
}




// Add alias
if (isset($_POST['vhost_name'])){
	
	if ($_POST['vhost_name'] == '') {
		header('Location: index.php?to=add_vhost&vhost_error=yes'); 
		exit;
	}
	
	// Add \ at the end if not present
	$vhost_path = rtrim( str_replace('\\', '/', $_POST['vhost_path']), '/' );

	$new_vhost[0]['vhost_link'] = urlencode(trim($vhost_path));
	$new_vhost[0]['vhost_name'] = trim($_POST['vhost_name']);	

	if (file_exists('store_vhosts.php')) {
		$vhost_serialized = file_get_contents('store_vhosts.php');
	} else {
		$vhost_serialized = serialize(array());
	}
	$vhost_array = array();


	if ($vhost_serialized != '') {
		$vhost_names = array_column(unserialize($vhost_serialized), 'vhost_name');
		if (in_array($new_vhost[0]['vhost_name'], $vhost_names)) {
			header('Location: index.php?to=add_vhost&vhost_error=exists'); 
			exit;
		} else {
			$vhost_array = array_merge(unserialize($vhost_serialized), $new_vhost);
		}
	} else {
		$vhost_array = $new_vhost;
	}

	file_put_contents('store_vhosts.php',serialize($vhost_array));
	
	// Restart http server if http server running
	if ($eds_httpserver_running == 1) include('../eds-binaries/httpserver/' . $conf_httpserver['httpserver_folder'] . '/eds-app-restart.php');	
	
	header('Location: index.php'); 
	exit;
}
<div class="row">
<div class="col-sm-10 col-sm-offset-1">
	<h3>Virtual Hosts</h3>	
	<p class="text-muted">Organise your working environment by listing vhosts below.</p>

	<?php
	// Add VHOST
	if (isset($_GET['to']) AND $_GET['to'] == 'add_vhost'){
		?>
		<br />
		
		<div class="row" style="background-color:#f7f7f7;border-radius:4px;">
		
			<div class="col-sm-6 col-sm-offset-3">
				<h3>Add Virtual Host</h3>
				<?php
				if (isset($_GET['alias_error']) AND ($_GET['alias_error'] == 'alias_exists')){
					echo '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-flash" aria-hidden="true"></span> This name is already taken!</div>';	
				}
				if (isset($_GET['alias_error']) AND ($_GET['alias_error'] == 'alias_name_empty')){
					echo '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-flash" aria-hidden="true"></span> Working directory name <b>empty</b></div>';
				}
				if (isset($_GET['alias_error']) AND ($_GET['alias_error'] == 'alias_path_empty')){
					echo '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-flash" aria-hidden="true"></span> Working directory path <b>empty</b></div>';
				}
				if (isset($_GET['alias_error']) AND ($_GET['alias_error'] == 'alias_nofolder')){
					echo '<div class="alert alert-warning" role="alert"><span class="glyphicon glyphicon-flash" aria-hidden="true"></span> This directory <b>doesn\'t exist</b></div>';
				}							
				
				?>
				<form method="post" action="index.php" role="form" id="alias-form">
					<div class="form-group">
						<label for="vhost_name"><span class="badge small">1</span> vHost name</label>
						<p style="color:silver;padding-left:25px;margin-bottom:0px;">Example: "dev.rdas.work"</p>
						<p style="color:silver;padding-left:25px;"><b>Note</b>: 
							make sure to add it windows host file. <br>
							e.g. 127.0.01 dev.rdas.work	
						</p>
						<div class="input-group" style="padding-left:25px;">
						<span class="input-group-addon input-sm"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span></span>
							<input type="text" class="form-control input-sm" id="vhost_name" name="vhost_name" placeholder="dev.rdas.work" />
						</div>
					</div>
					<div class="form-group">
						<label for="vhost_path"><span class="badge small">2</span> Path to the working directory</label>
						<p style="color:silver;padding-left:25px;">Example: "E:\Projects\easyphp"</p>
						<div class="input-group" style="padding-left:25px;">
							<span class="input-group-addon input-sm"><span class="glyphicon glyphicon-folder-open" aria-hidden="true"></span></span>
							<input type="text" class="form-control input-sm" id="vhost_path" name="vhost_path" placeholder="working directory path" />
						</div>
					</div>
					<div class="text-center"><button type="submit" onclick="delay()" class="btn btn-primary btn-sm">save</button> <a href="index.php" class="btn btn-default btn-sm">cancel</a></div>
				</form>	
				<br />
			</div>
		</div>					
		<?php
	}
	?>


	<ul class="list-group" style="margin-bottom:5px;">
			<?php
			if (file_exists('store_vhosts.php')) {
				$vhosts_serialized = file_get_contents('store_vhosts.php');
				if ($vhosts_serialized != '') {
					foreach (unserialize($vhosts_serialized) as $key => $vhost) {
						$exists = (file_exists(urldecode($vhost['vhost_link']))) ? true : false;
						?>
						<li class="list-group-item" style="padding:0px;">
							<table class="table table-condensed" style="padding:0px;margin-bottom:0px;">
							<tbody>
								<tr>
									<td>										
										<span class="glyphicon glyphicon-ban-circle" style="color:#95a5a6;opacity:0.5;" aria-hidden="true"></span>
									</td>
									<td style="width:30%">
										<a target="_blank" href="http://<?php echo urldecode($vhost['vhost_name']) ?>" class="list_alias_name" style="opacity:0.5" ><b><?php echo urldecode($vhost['vhost_name']) ?></b></a>
									</td>
									<td>
										<span class="glyphicon glyphicon-list small" aria-hidden="true" style="color:silver;padding-right:0px;"></span>
									</td>
									<td style="width:70%">
										<samp class="small" style="color: silver; padding-left: 0px;"><?php echo urldecode($vhost['vhost_link']) ?></samp>
									</td>
									<td style="padding-right: 10px;">										
										<form method="post" action="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" class="form-inline" role="form" style="display:inline" name="vhost_<?php echo $key ?>">
											<input type="hidden" name="vhost_delete" value="<?php echo $key ?>" />
											<a type="submit" role="submit" class="list_alias_directory" onclick="delay();document.forms['vhost_<?php echo $key ?>'].submit()"><samp class="small">Delete</samp></a>
										</form>											
									</td>
								</tr>
							</tbody>
							</table>
						</li>


								<?php
								if (file_exists(urldecode($vhost['vhost_link']))) {
									?>


									
									<!-- <td style="width:60%">
										<form method="post" action="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" class="form-inline" role="form" style="display:inline" name="alias_<?php echo $key ?>">
											<input type="hidden" name="action[request][0][type]" value="exe" />
											<input type="hidden" name="action[request][0][value]" value="<?php echo urlencode('explorer ' . urldecode($vhost['alias_path'])) ?>" />
											<a type="submit" role="submit" class="list_alias_directory"  data-toggle="tooltip" data-placement="top" title="Open in File Explorer" onclick="delay();document.forms['alias_<?php echo $key ?>'].submit()"><samp class="small"><?php echo wordwrap(urldecode($vhost['alias_path']), 50, "<br />", true); ?></samp></a>
										</form>		
									</td>
									<td class="text-right">
										<a role="button" tabindex="0" style="cursor:pointer;" class="delete_alias glyphicon glyphicon-remove" aria-hidden="true" data-toggle="popover" data-trigger="focus" data-html="true" data-placement="top" data-content="<div class='text-center'><a href='?delete_alias=<?php echo $key ?>' style='color:white;' class='btn btn-danger btn-sm'>delete</a></div>"></a>
									</td> -->
									<?php
								} else {
									// Directory doesn't exist
									?>
									<td>
										<span class="glyphicon glyphicon-eye-close" style="color:#e74c3c;opacity:0.5" aria-hidden="true"></span>
									</td>
									<td style="width:40%">														
										<span class="list_alias_name_broken" style="opacity:0.5" data-toggle="tooltip" data-placement="top" title="The folder doesn't exist. Check your folder or delete this working directory and create a new one."><b><?php echo wordwrap(substr($vhost['alias_name'],5), 20, "<br />", true); ?></b></span>
									</td>
									<td>	
										<span class="glyphicon glyphicon-list small" aria-hidden="true" style="color:silver;padding-right:0px;"></span>														
									</td>														
									<td style="width:60%">													
										<samp class="small" style="color:silver;"><?php echo wordwrap(urldecode($vhost['alias_path']), 50, "<br />", true); ?></samp>
									</td>
									<td class="text-right">
										<a role="button" tabindex="0" style="cursor:pointer;" class="delete_alias glyphicon glyphicon-remove" aria-hidden="true" data-toggle="popover" data-trigger="focus" data-html="true" data-placement="top" data-content="<div class='text-center'><a href='?delete_alias=<?php echo $key ?>' style='color:white;' class='btn btn-danger btn-sm'>delete</a></div>"></a>
									</td>
									<?php
								}
								?>
								</tr>
							</table>

							<div class="collapse" id="collapse_alias_<?php echo $key ?>">
								<iframe src="explorer.php?alias=<?php echo $vhost['alias_name'] ?>&root_dir=<?php echo urldecode($vhost['alias_path']) ?>" style="width:100%;" frameborder="0" scrolling="auto" height="400"></iframe>
							</div>												
							
						</li>
						<?php
					}
				}
			}
		?>
	</ul>
	<a href="?to=add_vhost" style="float:right" class="btn btn-default btn-xs" href="#" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> add vhost</a>
		<!-- <a href="?to=add_alias" style="float:right" class="btn btn-default btn-xs" href="#" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> add directory</a>
		<br />
		<br />
		<p class="text-muted">If you use a removable drive, store your files in the directory indicated below.</p>
		<ul class="list-group">
			<li class="list-group-item" style="padding:0px;">
				<table class="table table-condensed" style="padding:0px;margin-bottom:0px;">
					<tr>
						<?php
						if ($eds_httpserver_running == 1) {
							?>
							<td data-toggle="tooltip" data-placement="left" title="Expand">										
								<a data-toggle="collapse" href="#collapse_portabledirectory" aria-expanded="false" aria-controls="collapse_portabledirectory"><span class="glyphicon glyphicon-menu-hamburger" style="color:#95a5a6;" aria-hidden="true"></span></a>
							</td>
							<td style="width:30%">
								<a href="http://127.0.0.1:<?php echo $conf_httpserver['httpserver_port'] ?>" class="list_alias_name" data-toggle="tooltip" data-placement="top" title="Open in Browser"><b>Portable Directory</b></a>
							</td>
							<?php
						} else {
							?>
							<td>										
								<span class="glyphicon glyphicon-ban-circle" style="color:#95a5a6;opacity:0.5;" aria-hidden="true"></span>
							</td>
							<td style="width:30%">
								<span class="list_alias_name" style="opacity:0.5" data-toggle="tooltip" data-placement="top" title="Start an HTTP server!"><b>Portable Directory</b></span>
							</td>
							<?php
						}
						?>
						<td>
							<span class="glyphicon glyphicon-list small" aria-hidden="true" style="color:silver;padding-right:0px;"></span>
						</td>
						<td style="width:70%">
							<form method="post" action="http://<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" class="form-inline" role="form" style="display:inline" name="localweb">
								<input type="hidden" name="action[request][0][type]" value="exe" />
								<input type="hidden" name="action[request][0][value]" value="<?php echo urlencode('explorer ' . dirname(__DIR__) . '\eds-www') ?>" />
								<a type="submit" role="submit" class="list_alias_directory"  data-toggle="tooltip" data-placement="top" title="Open in File Explorer" onclick="delay();document.forms['localweb'].submit()"><samp class="small"><?php echo dirname(__DIR__)?>\eds-www</samp></a>
							</form>	
						</td>												
					</tr>
				</table>
				
				<div class="collapse" id="collapse_portabledirectory">
					<iframe src="explorer.php?root_dir=<?php echo urlencode(dirname(__DIR__).'\eds-www\\') ?>" style="width:100%;" frameborder="0" scrolling="auto" height="400"></iframe>
				</div>

			</li>
		</ul> -->
</div>
</div>